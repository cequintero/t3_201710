package model.logic;

import model.dara_structures.Queue;
import model.dara_structures.Stack;

public class ModeloNegocio {
	
	public boolean expresionBienFormada (String expresion)
	{
		if(darQuantityChar(expresion, "[")==darQuantityChar(expresion, "]") && darQuantityChar(expresion, "(")==darQuantityChar(expresion, ")"))
			return true;
		else
			return false;
	}
	
	public int darQuantityChar(String expresion, String character)
	{
		int position, count = 0;
		position = expresion.indexOf(character);
		while (position != -1){
			count++;
			position = expresion.indexOf(character, position+1);
		}
		return count;
	}
	
	public Stack ordenarPila(Stack temp)
	{
		Queue<Character> queuetemp = new Queue<Character>();
		while(temp.isEmpty()){
			queuetemp.enqueue((Character) temp.pop());}
		Stack<Character> stackaux1 = new Stack<Character>();
		Stack<Character> stackaux2 = new Stack<Character>();
		Stack<Character> stackaux3 = new Stack<Character>();
		while (queuetemp.isEmpty()) {
			char aux = (char) queuetemp.denqueue();
			if(aux=='['|| aux==']' || aux=='(' || aux==')')
				stackaux1.push(aux);
			else if(aux=='+'||aux=='-'||aux=='*'||aux=='/')
				stackaux2.push(aux);
			else if(aux=='0'||aux=='1'||aux=='2'||aux=='3'||aux=='4'||aux=='5'||aux=='6'||aux=='7'||aux=='8'||aux=='9')
				stackaux3.push(aux);
			System.out.println(queuetemp.size());
		}
		while(stackaux1.isEmpty())
			temp.push((Character)stackaux1.pop());
		while(stackaux2.isEmpty())
			temp.push((Character)stackaux1.pop());
		while(stackaux3.isEmpty())
			temp.push((Character)stackaux1.pop());
		
		return temp;
	}
	
	public Stack generarPila(String expresion)
	{
		Stack<Character> stackreturn = new Stack<Character>();
		char[] temp = expresion.toCharArray();
		for (int i = 0; i < temp.length; i++) {
			stackreturn.push(temp[i]);
		}
		return stackreturn;
	}
}
