package model.dara_structures;

public interface StackApi<T> {
	/** Crea una cola vac�a 
	 * @return */
	public void Stack();
	/** Agrega un item en la �ltima posici�n de la cola */
	//public void push(T elem);
	/** Elimina el elemento en la primera posici�n de la cola */
	public T pop();
	/** Indica si la cola est� vac�a */
	public boolean isEmpty();
	/** N�mero de elementos en la cola */
	public int size();
	void push(T elem);
}