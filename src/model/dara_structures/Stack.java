package model.dara_structures;

public class Stack<T> implements StackApi<T> {

	ListaEncadenada<T> stackx = new ListaEncadenada<T>();	
	public void Stack() {}

	@Override
	public void push(T elem) {
		stackx.agregarElementoFinal(elem);
	}

	@Override
	public T pop() {
		return stackx.eliminarElementoFinal();
	}

	@Override
	public boolean isEmpty() {
		if(stackx.darNumeroElementos()==0)
			return true;
		else
			return false;
	}

	@Override
	public int size() {
		return stackx.darNumeroElementos();
	}
}
