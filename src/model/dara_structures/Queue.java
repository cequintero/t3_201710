package model.dara_structures;

public class Queue<T> implements QueueApi<T> {

	ListaEncadenada<T> queuex = new ListaEncadenada<T>();
	@Override
	public void Queue() {}
		
	@Override
	public void enqueue(T item) {
		queuex.agregarElementoFinal(item);
		
	}

	@Override
	public T denqueue() {
		return queuex.eliminarElementoInicio();
	}

	@Override
	public boolean isEmpty() {
		if(queuex.darNumeroElementos()==0){
			return true;}
		else{
			return false;}
	}

	@Override
	public int size() {
		return queuex.darNumeroElementos();
	}

}
