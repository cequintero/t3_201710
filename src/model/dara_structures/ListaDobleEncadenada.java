package model.dara_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	NodoDoble<T> primera = null;
	NodoDoble<T> actualactual=primera;
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> actual = primera;
		if(actual == null)
			primera = new NodoDoble<T>(elem);

		else{
			while(actual != null && actual.siguiente != null){

				actual = actual.siguiente;
			}
			actual.siguiente = new NodoDoble<T>(elem);
			actual.siguiente.anterior = actual;
		}

	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		NodoDoble<T> actual = primera;
		for(;pos > 0 && actual != null; pos--)
			actual = actual.siguiente;
		return actual == null ? null : actual.elemento;

	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		NodoDoble<T> actual = primera;
		int pos = 0;
		for(; actual != null; actual = actual.siguiente, pos++){};
		return pos;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return (T) actualactual;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(actualactual.siguiente != null){
			actualactual = actualactual.siguiente;
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if(actualactual != primera){
			actualactual = actualactual.anterior;
			return true;
		}
		else{
			return false;
		}
	}

	public static class NodoDoble<T>
	{
		T elemento;
		NodoDoble<T> siguiente;
		NodoDoble<T> anterior;

		public NodoDoble(T elem) {
			this.elemento = elem;
		}
	}
}
