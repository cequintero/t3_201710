package model.dara_structures;

import java.util.Iterator;

import javax.print.attribute.standard.Sides;

import org.omg.CORBA.NO_IMPLEMENT;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

public class ListaEncadenada<T> implements ILista<T> {
	
	private NodoSencillo<T> primera =null;
	private NodoSencillo<T> actualactual =primera;
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> actual = primera;
		while(actual != null && actual.siguiente !=null){
			actual = actual.siguiente;
		}
		if (actual == null)
			primera = new NodoSencillo<T>(elem);
		else
			actual.siguiente = new NodoSencillo<T>(elem);
	}
	
	public T eliminarElementoFinal() {
		// TODO Auto-generated method stub
		NodoSencillo<T> retorno = null;
		NodoSencillo<T> actual = primera;
		while(actual != null && actual.siguiente.siguiente !=null){
				actual= actual.siguiente;
		}
		if (actual == null)
		{}
		else if(actual.siguiente == null)
		{
			retorno = primera;
			primera = null;
		}
		else
		{
			retorno = actual.siguiente;
			actual.siguiente = null;
		}
		return (T) retorno;
	}
	
	public T eliminarElementoInicio()
	{
		NodoSencillo<T> b = primera;
		if(primera!=null)
		{
			if(primera.siguiente!=null)
			{
				NodoSencillo<T> a = primera.siguiente;
				primera = a;
			}else
			{
				primera=null;
			}
		}
		return (T) b;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		
		NodoSencillo<T> actual = primera;
		for(;pos > 0 && actual != null; pos--)
			actual = actual.siguiente;
			return actual == null ? null : actual.elemento; 
		
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		NodoSencillo<T> actual = primera;
		int pos = 0;
		for(; actual != null; actual = actual.siguiente, pos++);
		System.out.println(pos);
		return pos;	
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return (T) actualactual;
		
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(actualactual.siguiente != null){
			actualactual = actualactual.siguiente;
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(actualactual==primera){
			return false;
		}else{
			NodoSencillo<T> actual = primera;
			while(actual.siguiente == actualactual){
				actual = actual.siguiente;
			}
			actualactual = actual;
			return true;
		}
		
	}
	
	public static class NodoSencillo <T>
	{
		T elemento;
		NodoSencillo<T> siguiente;
		

		public NodoSencillo(T elemento) {
			this.elemento = elemento;
		}
	}

}
