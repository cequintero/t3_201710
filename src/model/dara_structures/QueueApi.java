package model.dara_structures;

public interface QueueApi<T> {
	/** crea una cola vacia*/
	public void Queue();
	/** Agrega un item en la �ltima posici�n de la cola */
	/** Elimina el elemento en la primera posici�n de la cola*/
	public T denqueue();
	/** Indica si la cola est� vac�a*/
	public boolean isEmpty();
	/** N�mero de elementos en la cola*/
	public int size();
	void enqueue(T item);
}
