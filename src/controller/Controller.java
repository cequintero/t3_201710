package controller;

import model.dara_structures.Stack;
import model.logic.ModeloNegocio;

public class Controller {
	private static ModeloNegocio model = new ModeloNegocio();

	public static boolean darEstaBienFormada(String ecuacion) {
		return model.expresionBienFormada(ecuacion);
	}

	public static Stack darOrdenado(String ecuacion) {
		return model.ordenarPila(model.generarPila(ecuacion));
	}
}
