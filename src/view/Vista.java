package view;

import java.util.Scanner;

import controller.Controller;
import model.dara_structures.Stack;

public class Vista {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			String ecuacion = sc.next();
			if(Controller.darEstaBienFormada(ecuacion)){
				System.out.println("La ecuacion esta bien formada");
				Stack ss = Controller.darOrdenado(ecuacion);
				while (ss.isEmpty())
				{
					System.out.println(ss.pop());
				}
				break;
			}else{
				System.out.println("La ecuacion no esta bien formada");
				break;
			}
	}

}

	private static void printMenu() {
		System.out.println("---------Cesar Quintero----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("Incertar ecuacion");
	}
}
